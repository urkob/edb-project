package main

import (
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/urkob/kamlesh-edb-project/internal/bitmap"
)

func main() {
	rotateFeep()
}

func rotateFeep() {
	rotateAsset("feep.pbm", 90)
}

func rotateAsset(fileName string, angle float64) {
	img, err := bitmap.LoadPBM("./assets/" + fileName)
	if err != nil {
		log.Printf("loadPBM: %s\n", err)
		return
	}

	fileNameWithoutExtension := strings.Split(fileName, ".")[0]

	rand.Seed(time.Now().UnixNano())
	rotatedImg := img.RotateClockwise(angle)
	err = rotatedImg.SaveToFile(fmt.Sprintf("./assets/rotated/%s_%02f.pbm", fileNameWithoutExtension, angle))
	if err != nil {
		log.Printf("rotatedImg.SaveToFile: %s\n", err)
	}
}
