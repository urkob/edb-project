package bitmap

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/urkob/kamlesh-edb-project/pkg"
)

// LoadPBM from file and stores into *Image.
func LoadPBM(filename string) (pkg.BitmapIface, error) {
	return loadPBM(filename)
}

func loadPBM(filename string) (*Image, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	header := scanner.Text()
	width, height := 0, 0

	for {
		scanner.Scan()
		line := scanner.Text()
		if isComment(line) {
			continue
		}

		fmt.Sscanf(scanner.Text(), "%d %d", &width, &height)
		if width <= 0 || height <= 0 {
			return nil, fmt.Errorf("height %d | width %d", height, width)
		}
		break
	}

	im := newImage(header, width, height)
	row := 0
	for scanner.Scan() {
		line := scanner.Text()
		if isComment(line) {
			continue
		}
		line = strings.ReplaceAll(line, " ", "")
		for col, char := range line {
			paintPixel := char == '1'
			im.setPixel(row, col, paintPixel)
		}
		row++
	}
	return im, nil
}

// SaveToFile saves image as pbm file
func (img *Image) SaveToFile(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = fmt.Fprintln(file, img.Header)
	if err != nil {
		return fmt.Errorf("error while print header: %s", err)
	}

	_, err = fmt.Fprintf(file, "%d %d\n", img.Width, img.Height)
	if err != nil {
		return fmt.Errorf("error while print size: %s", err)
	}

	cell := ""
	for row := 0; row < img.Height; row++ {
		for col := 0; col < img.Width; col++ {
			if img.getPixel(row, col) {
				cell = "1"
			} else {
				cell = "0"
			}

			_, err = fmt.Fprint(file, cell+" ")
			if err != nil {
				return fmt.Errorf("error while print cell value: %s", err)
			}
		}

		// Prints newline
		_, err = fmt.Fprintln(file)
		if err != nil {
			return fmt.Errorf("error while print new line: %s", err)
		}
	}
	return nil
}
