package bitmap

import (
	"log"
	"math"

	"gitlab.com/urkob/kamlesh-edb-project/pkg"
)

// Image handles all properties and methods of a pbm file
type Image struct {
	// Pixels is the bitmap 2D matrix.
	Pixels [][]bool
	Width  int
	Height int
	Header string
}

// initializes a new image, set pixels to bounds within widht and height.
func newImage(header string, width, height int) *Image {
	if width <= 0 || height <= 0 {
		return nil
	}

	pixels := initPixels(height, width)
	return &Image{
		Header: header,
		Pixels: pixels,
		Width:  width,
		Height: height,
	}
}

func newP1Image(width, height int) *Image {
	if width <= 0 || height <= 0 {
		return nil
	}

	pixels := initPixels(height, width)
	return &Image{
		Header: "P1",
		Pixels: pixels,
		Width:  width,
		Height: height,
	}
}

func (img *Image) isInBounds(row, col int) bool {
	if len(img.Pixels) <= 0 || row < 0 || col < 0 {
		return false
	}

	if row > len(img.Pixels)-1 || col > len(img.Pixels[0])-1 {
		return false
	}

	return true
}

func (img *Image) setPixel(row, col int, paint bool) {
	img.Pixels[row][col] = paint
}

func (img *Image) getPixel(row, col int) bool {
	return img.Pixels[row][col]
}

// center return height center pont and width center point which could be translate as y, and x
func (img *Image) center() (float64, float64) {
	return float64(img.Height) / 2, float64(img.Width) / 2
}

// resize will give new bounds of drawable 2D matrix.
//
// With the help of this post I acquire the formula to solve the rotation resize algorythm: https://math.stackexchange.com/a/71069
//
// Where wh is height and ww is width
//
//	wh(d)=h|cos(d)|+w|sin(d)|
//	ww(d)=h|sin(d)|+w|cos(d)|
func (img *Image) resize(rotationAngle float64) *Image {
	rotationAngle = toRadians(rotationAngle)

	// wh(d)=h|cos(d)|+w|sin(d)|
	newHeight := math.Floor(
		(float64(img.Height) * math.Abs(math.Cos(rotationAngle))) +
			(float64(img.Width) * math.Abs(math.Sin(rotationAngle))),
	)

	// ww(d)=h|sin(d)|+w|cos(d)|
	newWidth := math.Floor(
		(float64(img.Height) * math.Abs(math.Sin(rotationAngle))) +
			(float64(img.Width) * math.Abs(math.Cos(rotationAngle))),
	)

	if newHeight < float64(img.Height) {
		newHeight = float64(img.Height)
	}

	if newWidth < float64(img.Width) {
		newWidth = float64(img.Width)
	}

	newImg := newP1Image(int(newWidth), int(newHeight))
	for row, rows := range img.Pixels {
		for col, cell := range rows {
			newRow := math.Round(float64(row) + math.Round((newHeight-float64(img.Height))/2))
			newCol := math.Round(float64(col) + math.Round((newWidth-float64(img.Width))/2))
			if !newImg.isInBounds(int(newRow), int(newCol)) {
				log.Println("this log should never comes here")
				continue
			}
			newImg.setPixel(int(newRow), int(newCol), cell)
		}
	}
	return newImg
}

// rotate
//
//	pxº = cos(theta) * (px-ox) - sin(theta) * (py-oy) + ox
//	pyº = sin(theta) * (px-ox) + cos(theta) * (py-oy) + oy
func (img *Image) rotate(rotationAngle float64) pkg.BitmapIface {
	rotationAngle = toRadians(rotationAngle)

	newImg := newImage(img.Header, img.Width, img.Height)
	rowCenter, colCenter := img.center()

	cos := math.Cos(rotationAngle)
	sin := math.Sin(rotationAngle)

	for rowIndex, cols := range img.Pixels {
		for colIndex, cellValue := range cols {
			if !cellValue {
				continue
			}
			colI := float64(colIndex)
			rowI := float64(rowIndex)

			newW := ((colI-colCenter)*cos - (rowI-rowCenter)*sin) + (colCenter)
			newH := ((colI-colCenter)*sin + (rowI-rowCenter)*cos) + (rowCenter)

			// I've decied to add 0.4 instead of using math.Floor or even math.Round
			nw := int(math.Round(newW + 0.4))
			nh := int(math.Round(newH + 0.4))
			if !newImg.isInBounds(nh, nw) {
				log.Println("this never should come here")
				continue
			}
			newImg.setPixel(nh, nw, cellValue)
		}
	}

	return newImg
}

// Rotate image clockwise (to the right).
func (img *Image) RotateClockwise(rotationAngle float64) pkg.BitmapIface {
	if rotationAngle < 0 {
		rotationAngle = rotationAngle * -1
	}

	return img.resize(rotationAngle).rotate(rotationAngle)
}

// It rotates image counter clockwise (to the left).
func (img *Image) RotateCounterClockwise(rotationAngle float64) pkg.BitmapIface {
	if rotationAngle > 0 {
		rotationAngle = rotationAngle * -1
	}
	return img.resize(rotationAngle).rotate(rotationAngle)
}
