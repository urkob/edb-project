package bitmap

import (
	"math"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

func rootDir() string {
	_, b, _, _ := runtime.Caller(0)
	d := path.Join(path.Dir(b))
	return filepath.Dir(d)
}

func isComment(line string) bool {
	return strings.HasPrefix(line, "#")
}

func initPixels(height int, width int) [][]bool {
	pixels := make([][]bool, height)
	for i := range pixels {
		pixels[i] = make([]bool, width)
	}
	return pixels
}

func toRadians(rotationAngle float64) float64 {
	return float64(rotationAngle) * (math.Pi / 180)
}
