package bitmap

import (
	"fmt"
	"log"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Input  string `required:"true" split_words:"true"`
	Output string `required:"true" split_words:"true"`
}

func NewConfig() *Config {
	envPath := fmt.Sprintf("%s/.env", filepath.Dir(rootDir()))
	err := godotenv.Load(envPath)
	if err != nil {
		log.Fatalf("environment variable ENV is empty and an error occurred while loading the .env file\n")
	}

	cfg := &Config{}
	err = envconfig.Process("", cfg)
	if err != nil {
		log.Fatalf("envconfig.Process: %s\n", err)
	}

	return cfg
}

var cfg *Config

func init() {
	cfg = NewConfig()
}

func TestNewImage(t *testing.T) {
	img := newImage("P1", 5, 5)

	require.NotNil(t, img, "image should not be nil, as we defined widht and height")
}

func TestNewImageNil(t *testing.T) {
	tests := map[int]int{
		0:  1,
		1:  0,
		-1: -1,
	}
	var img *Image
	for k, v := range tests {
		img = newImage("P1", k, v)
		assert.Nil(t, img, fmt.Sprintf("width: %d height: %c is error", k, v))
	}
}

func TestRotate(t *testing.T) {
	fileName := cfg.Input
	img, err := loadPBM(fileName)
	require.NoError(t, err)
	require.NotNil(t, img)

	rotationAngle := 45.0
	rotatedImg := img.rotate(rotationAngle)
	log.Println(rotatedImg)
}

var testAngles = []float64{
	90.0,
	180.0,
	270.0,
	360.0,
}

func TestResizeWithRotate(t *testing.T) {
	fileName := cfg.Input
	img, err := loadPBM(fileName)
	require.NoError(t, err)
	require.NotNil(t, img)

	for _, rotationAngle := range testAngles {
		newImg := img.resize(rotationAngle)

		require.NotNil(t, newImg)

		rotated := newImg.rotate(rotationAngle)
		require.NotNil(t, rotated)

		fileP := strings.Split(cfg.Output, ".")
		err := rotated.SaveToFile(fmt.Sprintf(fileP[0] + "TestResizeWithRotate_" + fmt.Sprint(rotationAngle) + "." + fileP[1]))
		require.NoError(t, err)

	}
}
