package pkg

type BitmapIface interface {
	RotateClockwise(rotationAngle float64) BitmapIface
	RotateCounterClockwise(rotationAngle float64) BitmapIface
	SaveToFile(path string) error
}
